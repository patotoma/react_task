import React from 'react';

import './CommentOptions.scss';

/**
 * CommentOptions
 */
class CommentOptions extends React.PureComponent {
  render() {
    const {
      likes,
      replies,
    } = this.props;

    return (
      <div className="comment-options-component">
        <a href="">Like<span>&nbsp;({likes})</span></a>
        <a href="">Reply</a>
        <a href="">View Replies<span>&nbsp;({replies})</span></a>
      </div>
    );
  }
}

export default CommentOptions;
