import React from 'react';

import './Comment.scss';

import CommentPhoto from './photo/CommentPhoto.js';
import CommentContent from './content/CommentContent.js';
import CommentOptions from './options/CommentOptions.js';

/**
 * Comment
 */
class Comment extends React.PureComponent {
  render() {
    const {
      comment,
    } = this.props;

    return (
      <div className={`comment-component ${comment.id === 'fake' ? 'fake' : ''}`}>

        <div className="content">
          <CommentPhoto />

          <CommentContent
            user={comment.user}
            message={comment.message}
          />
        </div>

        <CommentOptions
          likes={comment.likes}
          replies={comment.replies}
        />
      </div>
    );
  }
}

export default Comment;
