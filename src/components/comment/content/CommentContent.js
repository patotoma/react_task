import React from 'react';

import './CommentContent.scss';

/**
 * CommentContent
 */
class CommentContent extends React.PureComponent {
  render() {
    const {
      user,
      message
    } = this.props;

    return (
      <div className="comment-content-component">
        <div className="username">{user.name}</div>
        <div className="message">{message}</div>
      </div>
    );
  }
}

export default CommentContent;
