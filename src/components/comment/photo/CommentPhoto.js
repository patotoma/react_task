import React from 'react';

import './CommentPhoto.scss';

/**
 * CommentPhoto
 */
class CommentPhoto extends React.PureComponent {
  render() {
    return (
      <div className="comment-photo-component">
        <img className="photo" src={require('../../../assets/doggy.jpg')} alt=""/>
      </div>
    );
  }
}

export default CommentPhoto;
