import * as appActions from './actions.js';

const initialState = {
  loading: false,
  error: false,
  comments: [
    {
      id: 1,
      user: {
        id: 1,
        name: 'Janet Ryan'
      },
      message: 'I really like...',
      likes: 15,
      replies: 2,
    },
    {
      id: 2,
      user: {
        id: 2,
        name: 'Alice Kelly'
      },
      message: 'Have you ever tried...',
      likes: 5,
      replies: 1,
    },
  ],
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {

  case appActions.COMMENT_CREATE_START: {
    return {
      loading: true,
      error: false,
      comments: [action.payload.comment].concat(state.comments),
    };
  }

  case appActions.COMMENT_CREATE_SUCCESS: {
    return {
      loading: false,
      error: false,
      comments: state.comments.map(comment =>
        comment.id !== 'fake' ? comment : action.payload.comment
      ),
    };
  }

  case appActions.COMMENT_CREATE_ERROR: {
    return {
      loading: false,
      error: true,
      comments: state.comments.filter(comment =>
        comment.id !== 'fake'
      ),
    };
  }

  default:
    return state;
  }
}
