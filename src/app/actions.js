import {commentPost} from '../services/fakeApi.js';

/*
 *
 * COMMENT_CREATE action
 */
export const COMMENT_CREATE_START = 'COMMENT_CREATE_START';
export const COMMENT_CREATE_SUCCESS = 'COMMENT_CREATE_SUCCESS';
export const COMMENT_CREATE_ERROR = 'COMMENT_CREATE_ERROR';
export function commentCreate(message) {
  return dispatch => {
    // start dispatch
    dispatch({
      type: COMMENT_CREATE_START,
      payload: {
        comment: {
          id: 'fake',
          user: {
            id: 120,
            name: 'Dmytro Ulyanets',
          },
          message: message,
          likes: 0,
          replies: 0,
        },
      },
    });

    commentPost(message, response => {
      if (response.error) {
        // error dispatch
        dispatch({
          type: COMMENT_CREATE_ERROR,
          payload: response,
        });
      } else {
        // success dispatch
        dispatch({
          type: COMMENT_CREATE_SUCCESS,
          payload: response,
        });
      }
    });
  };
}
