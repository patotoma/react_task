import React from 'react';
import {connect} from 'react-redux';

import './App.scss';

import * as appActions from './actions.js';

import Comment from '../components/comment/Comment.js';
import CommentPhoto from '../components/comment/photo/CommentPhoto.js';

/**
 * Main Wrapper
 */
class App extends React.PureComponent {

    constructor() {
      super();

      // bind this context to custom class methods
      this.submit = this.submit.bind(this);
    }

    componentWillReceiveProps(nextProps) {
      if (
        this.props.app.loading
        && !nextProps.app.loading
        && !nextProps.app.error
      ) {
        // after successfully posted comment
        this._commentInput.value = ''; // reset input
      }
    }

    render() {
      const {
        app,
      } = this.props;

      return (
        <div className="main-wrapper">
          <div className="title">Comments</div>

          {/* comment form */}
          <div className="comment-form">
            <CommentPhoto />

            <div className="input-wrapper">
              <form action="" onSubmit={this.submit}>
                <input
                  ref={(c) => { this._commentInput = c; }}
                  className="input"
                  type="text"
                  placeholder="Add a comment..."
                  readOnly={app.loading}
                />
                {app.error &&
                  <small className="error">Something went wrong... Please try again later!</small>
                }
              </form>
            </div>
          </div>

          <hr/>

          {/* comments list */}
          <div className="comments">
            {app.comments.map(comment =>
              <Comment
                key={comment.id}
                comment={comment}
              />
            )}
          </div>

        </div>
      );
    }

    submit(e) {
      e.preventDefault();

      // don't submit empty comment
      if (this._commentInput.value === '') {
        return;
      }

      // don't submit while waiting for response
      if (this.props.app.loading) {
        return;
      }

      // dispatch action
      this.props.dispatch(appActions.commentCreate(
        this._commentInput.value
      ));
    }
}

export default connect(state => ({
  app: state.app,
}))(App);
