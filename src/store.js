import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import thunk from 'redux-thunk';
import {routerReducer, routerMiddleware} from 'react-router-redux';
import {hashHistory} from 'react-router';

import appReducer from './app/reducer.js';

const initialState = {};

const Store = createStore(
  combineReducers({
    routing: routerReducer,
    app: appReducer,
  }),
  initialState,
  compose(
    applyMiddleware(
      routerMiddleware(hashHistory),
      thunk,
    )
  )
);

export default Store;
