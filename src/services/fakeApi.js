// generate random time between 1000 and 3000
function generateRandomResponseTime() {
  return Math.floor(Math.random() * 3 + 1) * 1000;
}

export function commentPost(message, callback) {
  console.log('fake API received message', message);

  let response = {
    error: {
      code: '500',
      message: 'Cannot connect to database',
    },
  };
  if (message.length <= 25) {
    response = {
      comment: {
        id: Math.random(),
        user: {
          id: Math.random(),
          name: 'Dmytro Ulyanets'
        },
        message: message,
        likes: 0,
        replies: 0,
      }
    };
  }

  // invoke callback after random time (async-like)
  setTimeout(() => {
    callback(response);
  }, generateRandomResponseTime());
}
